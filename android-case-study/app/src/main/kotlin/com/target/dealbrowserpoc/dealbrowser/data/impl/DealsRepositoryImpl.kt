package com.target.dealbrowserpoc.dealbrowser.data.impl

import com.target.dealbrowserpoc.dealbrowser.data.api.Deal
import com.target.dealbrowserpoc.dealbrowser.data.api.DealsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DealsRepositoryImpl(private val dealsService: DealsService) : DealsRepository {
  override suspend fun getDeals(): List<Deal> {
    return withContext(Dispatchers.Default) {
      dealsService.deals().data.map { dto ->
        Deal(
          index = dto.index,
          id = dto.id,
          title = dto.title,
          // make description a little longer to demonstrate scrolling
          description = List(3) {dto.description }.joinToString("\n\n"),
          originalPrice = dto.originalPrice,
          salePrice = dto.salePrice,
          // the original placeholder images are hosted on a super slow server, this works better
          imageUrl = "https://picsum.photos/id/${50 + dto.index}/400/400",
          aisle = dto.aisle
        )
      }.sortedBy { it.index }
    }
  }
}