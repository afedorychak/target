package com.target.dealbrowserpoc.dealbrowser.common

enum class DealListDisplayMode {
  GRID, LIST
}