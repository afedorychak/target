package com.target.dealbrowserpoc.dealbrowser.dealdetails

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.ViewTreeObserver
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import com.target.dealbrowserpoc.dealbrowser.R

class ParallaxImageView @JvmOverloads constructor(
  context: Context?,
  attrs: AttributeSet? = null,
  defStyleAttr: Int = 0
) : ImageView(context, attrs, defStyleAttr), ViewTreeObserver.OnPreDrawListener {

  private val minVisiblePortionToHideScrim = 0.3f

  private var visiblePortion: Float = 1f
    set(value) {
      if (field != value) {
        field = value
        invalidate()
      }
    }

  private var tmpRect = Rect()
  private var scrimPaint = Paint().apply {
    color = ResourcesCompat.getColor(resources, R.color.actionbar_color, context?.theme)
  }

  override fun onAttachedToWindow() {
    super.onAttachedToWindow()
    viewTreeObserver.addOnPreDrawListener(this)
  }

  override fun onDetachedFromWindow() {
    viewTreeObserver.addOnPreDrawListener(this)
    super.onDetachedFromWindow()
  }

  override fun onDraw(canvas: Canvas) {
    // create parallax effect
    val parallaxOffset = 0.5f * (1 - visiblePortion) * height

    if (parallaxOffset > 0f) {
      // sometimes when you fling down and it scrolls to a hard stop something else (scroll view?) is messing
      // with how getGlobalVisibleRect calculates the rect and we get to see an empty part of the canvas (which
      // creates a flashing gap between toolbar and the image); for now, fill that area with opaque scrim color to
      // minimize the artifact
      tmpRect.set(0, 0, width, height / 2)
      scrimPaint.alpha = 255
      canvas.drawRect(tmpRect, scrimPaint)

      canvas.translate(0f, parallaxOffset)
    }

    super.onDraw(canvas)

    // draw scrim if needed
    val scrimAlpha = if (visiblePortion >= minVisiblePortionToHideScrim) 0f else (1 - visiblePortion / minVisiblePortionToHideScrim)

    if (scrimAlpha > 0) {
      tmpRect.set(0, 0, width, height)
      scrimPaint.alpha = (scrimAlpha * 255).toInt()
      canvas.drawRect(tmpRect, scrimPaint)
    }
  }

  override fun onPreDraw(): Boolean {
    if (getGlobalVisibleRect(tmpRect) && height > 0) {
      visiblePortion = tmpRect.height().toFloat() / height

      if (visiblePortion < 1 && height > resources.displayMetrics.heightPixels) {
        visiblePortion = 1f
      }
    }
    else {
      visiblePortion = 0f
    }

    return true
  }

}
