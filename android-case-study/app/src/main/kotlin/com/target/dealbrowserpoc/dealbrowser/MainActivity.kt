package com.target.dealbrowserpoc.dealbrowser

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.target.dealbrowserpoc.dealbrowser.dealdetails.DealDetailsFragment
import com.target.dealbrowserpoc.dealbrowser.deallist.DealsListFragment

class MainActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_main)

    if (savedInstanceState == null) {
      supportFragmentManager.beginTransaction()
        .add(R.id.container, DealsListFragment())
        .commit()
    }

    supportFragmentManager.registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
      override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {
        if (f.id == R.id.container) {
          updateTitle(f)
        }
      }
    }, false)

  }

  private fun updateTitle(currentFragment: Fragment) {
    val title = when (currentFragment) {
      is DealsListFragment -> resources.getString(R.string.app_name)
      is DealDetailsFragment -> resources.getString(R.string.details_title)
      else -> resources.getString(R.string.app_name)
    }

    setTitle(title)
  }
}