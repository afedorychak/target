package com.target.dealbrowserpoc.dealbrowser.dealdetails

import android.text.SpannableString
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.target.dealbrowserpoc.dealbrowser.R
import com.target.dealbrowserpoc.dealbrowser.data.api.Deal

class DealDetailsUi(inflater: LayoutInflater, container: ViewGroup?) {

  val view: View = inflater.inflate(R.layout.deal_details, container, false)

  private val image = view.findViewById<ImageView>(R.id.deal_image)
  private val originalPrice = view.findViewById<TextView>(R.id.orig_price)
  private val currentPrice = view.findViewById<TextView>(R.id.current_price)
  private val title = view.findViewById<TextView>(R.id.deal_title)
  private val description = view.findViewById<TextView>(R.id.deal_description)
  private val addToList = view.findViewById<Button>(R.id.add_to_list)
  private val addToCart = view.findViewById<Button>(R.id.add_to_cart)

  fun show(deal: Deal) {
    Glide.with(view.context)
      .load(deal.imageUrl)
      .into(image)

    showPrices(deal)

    title.text = deal.title
    description.text = deal.description

    listOf(addToList, addToCart).forEach { v ->
      v.setOnClickListener {
        view.context.applicationContext.let { Toast.makeText(it, "Not implemented", Toast.LENGTH_SHORT).show() }
      }
    }
  }

  private fun showPrices(deal: Deal) {
    if (deal.salePrice != null) {
      currentPrice.text = deal.salePrice
      currentPrice.setTextColor(ResourcesCompat.getColor(view.resources, R.color.sale_price_color, view.context.theme))
      originalPrice.isVisible = true
      setOriginalPriceAmount(deal.originalPrice)
    }
    else {
      currentPrice.text = deal.originalPrice
      currentPrice.setTextColor(ResourcesCompat.getColor(view.resources, R.color.default_text_color, view.context.theme))
      originalPrice.isVisible = false
    }
  }

  private fun setOriginalPriceAmount(formattedAmount: String) {
    val prefix = view.resources.getString(R.string.regular_price_prefix)
    originalPrice.text = SpannableString(prefix + formattedAmount).apply {
      setSpan(StrikethroughSpan(), prefix.length, length, 0)
    }
  }

}