package com.target.dealbrowserpoc.dealbrowser.deallist

import android.os.Bundle
import android.transition.AutoTransition
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.target.dealbrowserpoc.dealbrowser.R
import com.target.dealbrowserpoc.dealbrowser.common.DealListDisplayMode
import com.target.dealbrowserpoc.dealbrowser.common.app
import com.target.dealbrowserpoc.dealbrowser.common.viewModelFactory
import com.target.dealbrowserpoc.dealbrowser.data.api.Deal
import com.target.dealbrowserpoc.dealbrowser.dealdetails.DealDetailsFragment


class DealsListFragment : Fragment(), DealsListUiListener {
  private var ui: DealsListUi? = null

  init {
    reenterTransition = AutoTransition().excludeChildren(R.id.deals_recycler, true)
  }

  private val viewModel: DealsListViewModel by viewModelFactory { app ->
    DealsListViewModel(app.dealsRepository)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    setHasOptionsMenu(true)
    viewModel.state.observe(viewLifecycleOwner) { state ->
      ui?.show(state)
    }
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
    menu.add(Menu.NONE, Menu.NONE, Menu.NONE, "toggle").apply {
      setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
      val currentMode = ui?.displayMode ?: return
      val nextDisplayMode = ((currentMode.ordinal + 1) % DealListDisplayMode.values().size).let { DealListDisplayMode.values()[it] }

      title = when (nextDisplayMode) {
        DealListDisplayMode.GRID -> getString(R.string.show_as_grid)
        DealListDisplayMode.LIST -> getString(R.string.show_as_list)
      }

      setOnMenuItemClickListener {
        ui?.displayMode = nextDisplayMode
        context?.app?.preferredDisplayMode = nextDisplayMode
        activity?.invalidateOptionsMenu()
        true
      }
    }
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    ui = DealsListUi(inflater, container).apply {
      displayMode = context?.app?.preferredDisplayMode ?: displayMode
    }
    return ui?.view
  }

  override fun onStart() {
    super.onStart()
    ui?.listener = this
  }

  override fun onStop() {
    super.onStop()
    ui?.listener = null
  }

  override fun onDestroyView() {
    super.onDestroyView()
    ui = null
  }

  override fun onDealClick(deal: Deal) {
    val detailsFragment = DealDetailsFragment().also { f -> f.deal = deal }

    parentFragmentManager.beginTransaction()
      .replace(id, detailsFragment)
      .addToBackStack("show_details")
      .commit()
  }

  override fun onRetryLoad() {
    viewModel.loadDeals()
  }
}