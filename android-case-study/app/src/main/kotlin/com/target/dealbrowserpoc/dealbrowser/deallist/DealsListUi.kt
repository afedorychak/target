package com.target.dealbrowserpoc.dealbrowser.deallist

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.target.dealbrowserpoc.dealbrowser.R
import com.target.dealbrowserpoc.dealbrowser.common.AsyncStatus
import com.target.dealbrowserpoc.dealbrowser.common.DealListDisplayMode
import com.target.dealbrowserpoc.dealbrowser.common.DealListDisplayMode.GRID
import com.target.dealbrowserpoc.dealbrowser.common.DealListDisplayMode.LIST
import com.target.dealbrowserpoc.dealbrowser.common.ScreenConfig.*
import com.target.dealbrowserpoc.dealbrowser.common.screenConfig
import com.target.dealbrowserpoc.dealbrowser.data.api.Deal

interface DealsListUiListener {
  fun onDealClick(deal: Deal)
  fun onRetryLoad()
}

class DealsListUi(inflater: LayoutInflater, container: ViewGroup?) {

  var listener: DealsListUiListener? = null
  val view: View = inflater.inflate(R.layout.deals_list, container, false)
  var displayMode: DealListDisplayMode = LIST
    set(value) {
      if (field != value) {
        field = value
        applyDisplayMode()
      }
    }

  private val adapter = DealsListAdapter(useGridItems = false, onItemClick = { listener?.onDealClick(it) })
  private val dividerDecoration = DividerItemDecoration(view.context, RecyclerView.VERTICAL)
  private val gridSpacing = GridItemsSpaceDecoration(spacePx = 0)

  @Suppress("unused")
  private val recycler = view.findViewById<RecyclerView>(R.id.deals_recycler).also { rv ->
    rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(rv.context, R.anim.layout_animation_fall_down)
    rv.adapter = adapter
  }

  private val progressBar = view.findViewById<ProgressBar>(R.id.deals_list_progress)
  private val errorView = view.findViewById<ViewGroup>(R.id.error_view).apply {
    findViewById<View>(R.id.try_again).setOnClickListener {
      listener?.onRetryLoad()
    }
  }

  init {
    recycler.isVisible = false
    progressBar.isVisible = false
    errorView.isVisible = false
    applyDisplayMode()
  }

  private fun applyDisplayMode() {
    val screenConfig = view.context.screenConfig

    val columns = when (screenConfig) {
      SMALL_PORTRAIT -> if (displayMode == GRID) 2 else 1
      PORTRAIT -> if (displayMode == GRID) 3 else 1
      SMALL_LANDSCAPE -> if (displayMode == GRID) 2 else 1
      LANDSCAPE -> if (displayMode == GRID) 3 else 2
    }

    adapter.useGridItems = when (screenConfig) {
      SMALL_LANDSCAPE -> false
      else -> displayMode == GRID
    }

    gridSpacing.spacePx = when (screenConfig) {
      PORTRAIT, LANDSCAPE -> view.resources.getDimensionPixelSize(R.dimen.space_between_deals_in_grid_on_tablets)
      SMALL_PORTRAIT -> view.resources.getDimensionPixelSize(R.dimen.space_between_deals_in_grid_on_phones)
      else -> 0
    }

    while (recycler.itemDecorationCount > 0) {
      recycler.removeItemDecorationAt(0)
    }

    if (columns == 1) {
      recycler.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
      recycler.addItemDecoration(dividerDecoration)
    }
    else {
      recycler.layoutManager = GridLayoutManager(view.context, columns, RecyclerView.VERTICAL, false)
      recycler.addItemDecoration(gridSpacing)
    }

    recycler.recycledViewPool.clear()
    recycler.scheduleLayoutAnimation()
  }

  fun show(state: DealsListState) {
    recycler.isVisible = state.status == AsyncStatus.Success
    progressBar.isVisible = state.status == AsyncStatus.Working
    errorView.isVisible = state.status == AsyncStatus.Failure

    if (state.status != AsyncStatus.Working) {
      adapter.submitList(state.items)
    }
    else {
      adapter.submitList(null)
    }
  }

  private inner class GridItemsSpaceDecoration(var spacePx: Int) : RecyclerView.ItemDecoration() {
    private lateinit var layoutManager: GridLayoutManager

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
      if (!this::layoutManager.isInitialized || layoutManager !== parent.layoutManager) {
        layoutManager = parent.layoutManager as GridLayoutManager
      }

      val adapterPos = parent.getChildAdapterPosition(view)
      if (adapterPos == RecyclerView.NO_POSITION) {
        return
      }

      val spanIdx = layoutManager.spanSizeLookup.getSpanIndex(adapterPos, layoutManager.spanCount)
      val groupIdx = layoutManager.spanSizeLookup.getSpanGroupIndex(adapterPos, layoutManager.spanCount)

      val half = spacePx / 2
      outRect.set(half, half, half, half)

      // it's on the left
      if (spanIdx == 0) outRect.left += half
      // it's on the top
      if (groupIdx == 0) outRect.top += half
      // it's on the right
      if (spanIdx + 1 >= layoutManager.spanCount) outRect.right += half
      // it's on the bottom
      if ((adapter.itemCount / layoutManager.spanCount) == groupIdx) outRect.bottom += half
    }
  }
}

private class DealsListAdapter(
  var useGridItems: Boolean,
  private val onItemClick: (Deal) -> Unit
) : ListAdapter<Deal, DealItemViewHolder>(DealsListDiffer()) {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealItemViewHolder {
    val layoutId = if (useGridItems) R.layout.deal_grid_item else R.layout.deal_list_item
    return LayoutInflater.from(parent.context)
      .inflate(layoutId, parent, false)
      .let { DealItemViewHolder(it, onItemClick) }
  }

  override fun onBindViewHolder(holder: DealItemViewHolder, position: Int) {
    holder.bind(getItem(position))
  }
}

private class DealItemViewHolder(itemView: View, private val onClick: (Deal) -> Unit) : RecyclerView.ViewHolder(itemView) {

  private val productImage = itemView.findViewById<ImageView>(R.id.deal_list_item_image_view)
  private val title = itemView.findViewById<TextView>(R.id.deal_list_item_title)
  private val price = itemView.findViewById<TextView>(R.id.deal_list_item_price)
  private val aisle = itemView.findViewById<TextView>(R.id.aisle)

  fun bind(item: Deal) {
    itemView.setOnClickListener {
      onClick(item)
    }

    val dp = itemView.resources.displayMetrics.density
    Glide.with(itemView.context)
      .load(item.imageUrl)
      .transform(RoundedCorners((4 * dp).toInt()))
      .transition(DrawableTransitionOptions.withCrossFade())
      .into(productImage)

    title.text = item.title
    price.text = item.salePrice ?: item.originalPrice

    if (item.aisle.isNotBlank()) {
      aisle.isVisible = true
      aisle.text = item.aisle
    }
    else {
      aisle.isVisible = false
    }
  }
}

private class DealsListDiffer : DiffUtil.ItemCallback<Deal>() {
  override fun areItemsTheSame(oldItem: Deal, newItem: Deal): Boolean {
    return oldItem.id == newItem.id
  }

  override fun areContentsTheSame(oldItem: Deal, newItem: Deal): Boolean {
    return oldItem == newItem
  }
}

