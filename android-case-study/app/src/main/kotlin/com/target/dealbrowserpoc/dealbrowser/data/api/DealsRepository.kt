package com.target.dealbrowserpoc.dealbrowser.data.api

interface DealsRepository {
  suspend fun getDeals(): List<Deal>
}