package com.target.dealbrowserpoc.dealbrowser.common

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.target.dealbrowserpoc.dealbrowser.DealBrowserApplication

inline fun <reified T : ViewModel> Fragment.viewModelFactory(crossinline provider: (DealBrowserApplication) -> T): Lazy<T> {
  return viewModels {
    object : ViewModelProvider.Factory {
      override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return provider(context!!.app) as T
      }
    }
  }
}

inline val Context.screenConfig: ScreenConfig
  get() = resources.configuration.run {
    if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
      if (smallestScreenWidthDp < 600) ScreenConfig.SMALL_LANDSCAPE else ScreenConfig.LANDSCAPE
    }
    else {
      if (smallestScreenWidthDp < 600) ScreenConfig.SMALL_PORTRAIT else ScreenConfig.PORTRAIT
    }
  }

inline val Fragment.args: Bundle
  get() {
    if (arguments == null) {
      arguments = Bundle()
    }
    return arguments!!
  }

val Context.app get() = applicationContext as DealBrowserApplication

