package com.target.dealbrowserpoc.dealbrowser.data.impl

import com.google.gson.annotations.SerializedName
import retrofit2.http.GET

interface DealsService {
  @GET("/api/deals")
  suspend fun deals(): DealsDto
}

data class DealsDto(
  @SerializedName("data")
  val data: List<DealDto> = emptyList()
)

data class DealDto(
  @SerializedName("_id")
  val id: String = "",
  @SerializedName("index")
  val index: Int = 0,
  @SerializedName("title")
  val title: String = "",
  @SerializedName("description")
  val description: String = "",
  @SerializedName("price")
  val originalPrice: String = "",
  @SerializedName("salePrice")
  val salePrice: String? = null,
  @SerializedName("image")
  val imageUrl: String = "",
  @SerializedName("aisle")
  val aisle: String = ""
)