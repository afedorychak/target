package com.target.dealbrowserpoc.dealbrowser.common

enum class AsyncStatus {
  Working, Success, Failure
}