package com.target.dealbrowserpoc.dealbrowser

import android.app.Application
import com.target.dealbrowserpoc.dealbrowser.data.api.DealsRepository
import com.target.dealbrowserpoc.dealbrowser.data.impl.DealsRepositoryImpl
import com.target.dealbrowserpoc.dealbrowser.data.impl.DealsService
import com.target.dealbrowserpoc.dealbrowser.common.DealListDisplayMode
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Suppress("MemberVisibilityCanBePrivate")
class DealBrowserApplication : Application() {

  val dealsService: DealsService by lazy {
    Retrofit.Builder()
      .baseUrl("https://target-deals.herokuapp.com")
      .addConverterFactory(GsonConverterFactory.create())
      .build()
      .create(DealsService::class.java)
  }

  val dealsRepository: DealsRepository by lazy {
    DealsRepositoryImpl(dealsService)
  }

  var preferredDisplayMode: DealListDisplayMode = DealListDisplayMode.LIST

}