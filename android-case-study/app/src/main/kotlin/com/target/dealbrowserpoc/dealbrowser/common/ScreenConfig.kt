package com.target.dealbrowserpoc.dealbrowser.common

enum class ScreenConfig {
  SMALL_PORTRAIT, PORTRAIT, SMALL_LANDSCAPE, LANDSCAPE;

  val isSmallScreen get() = this == SMALL_PORTRAIT || this == SMALL_LANDSCAPE
}