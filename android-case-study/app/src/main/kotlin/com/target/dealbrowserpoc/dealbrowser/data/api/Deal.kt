package com.target.dealbrowserpoc.dealbrowser.data.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Deal(
  val id: String,
  val index: Int,
  val title: String,
  val description: String,
  val originalPrice: String,
  val salePrice: String?,
  val imageUrl: String,
  val aisle: String
) : Parcelable