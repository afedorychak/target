package com.target.dealbrowserpoc.dealbrowser.deallist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.target.dealbrowserpoc.dealbrowser.common.AsyncStatus
import com.target.dealbrowserpoc.dealbrowser.data.api.DealsRepository
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch

class DealsListViewModel(private val dealsRepository: DealsRepository) : ViewModel() {

  val state: LiveData<DealsListState> get() = _state

  private val _state = MutableLiveData<DealsListState>()

  init {
    loadDeals()
  }

  fun loadDeals() {
    viewModelScope.launch {
      _state.value = DealsListState(AsyncStatus.Working)
      try {
        val deals = dealsRepository.getDeals()
        _state.value = DealsListState(AsyncStatus.Success, deals)
      }
      catch (e: Exception) {
        if (e is CancellationException) throw e
        _state.value = DealsListState(AsyncStatus.Failure)
      }

    }
  }

}