package com.target.dealbrowserpoc.dealbrowser.deallist

import com.target.dealbrowserpoc.dealbrowser.common.AsyncStatus
import com.target.dealbrowserpoc.dealbrowser.data.api.Deal

data class DealsListState(
  val status: AsyncStatus,
  val items: List<Deal> = emptyList()
)