package com.target.dealbrowserpoc.dealbrowser.dealdetails

import android.content.Context
import android.os.Bundle
import android.transition.AutoTransition
import android.transition.Slide
import android.transition.TransitionSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.target.dealbrowserpoc.dealbrowser.R
import com.target.dealbrowserpoc.dealbrowser.common.args
import com.target.dealbrowserpoc.dealbrowser.data.api.Deal

class DealDetailsFragment : Fragment() {
  private var ui: DealDetailsUi? = null

  init {
    enterTransition = TransitionSet()
      .addTransition(AutoTransition().excludeTarget(R.id.deal_actions_bar, true))
      .addTransition(Slide().addTarget(R.id.deal_actions_bar))
  }

  var deal: Deal?
    set(value) {
      args.putParcelable("deal", value)
    }
    get() = args.getParcelable("deal")

  override fun onAttach(context: Context) {
    super.onAttach(context)
    if (deal == null) {
      error("Deal wasn't supplied")
    }
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    ui = DealDetailsUi(inflater, container)
    return ui?.view
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    ui?.show(deal ?: return)
  }

  override fun onDestroyView() {
    super.onDestroyView()
    ui = null
  }

}